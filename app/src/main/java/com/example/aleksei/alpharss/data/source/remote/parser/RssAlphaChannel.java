package com.example.aleksei.alpharss.data.source.remote.parser;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

@Root(strict = false)
public class RssAlphaChannel implements Serializable {

    @ElementList(name = "item", required = true, inline = true)
    private List<RssAlphaItem> itemList;

    public List<RssAlphaItem> getLentaItemList() {
        return itemList;
    }

    public void setItemList(List<RssAlphaItem> itemList) {
        this.itemList = itemList;
    }

    public RssAlphaChannel(List<RssAlphaItem> mFeedItems) {
        this.itemList = mFeedItems;
    }

    public RssAlphaChannel() {

    }
}
