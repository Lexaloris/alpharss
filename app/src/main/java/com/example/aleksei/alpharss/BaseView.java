package com.example.aleksei.alpharss;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
