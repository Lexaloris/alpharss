package com.example.aleksei.alpharss.splash;

import com.example.aleksei.alpharss.BasePresenter;
import com.example.aleksei.alpharss.BaseView;

public interface SplashContract {

    interface View extends BaseView<Presenter> {

        void setLoadingIndicator(boolean b);

        void showLoadingTidingsError();

        void showMainScreen();

        void showEnterForms();

        boolean isInternetAvailable();

        void showNoInternet();
    }

    interface Presenter extends BasePresenter {

        void loadTidings(boolean forceUpdate, final boolean showLoadingUI);

    }
}
