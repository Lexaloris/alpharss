package com.example.aleksei.alpharss.tidings;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aleksei.alpharss.R;
import com.example.aleksei.alpharss.data.Tiding;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TidingListAdapter extends RecyclerView.Adapter<TidingListAdapter.TidingViewHolder> {

    private List<Tiding> mTidings;

    private TidingsListFragment.TidingItemListener mItemListener;

    TidingListAdapter(List<Tiding> tidings, TidingsListFragment.TidingItemListener itemListener) {
        mTidings = tidings;
        mItemListener = itemListener;
    }

    @Override
    public TidingViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.tiding_item, viewGroup, false);
        return new TidingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TidingViewHolder holder, int position) {
        Tiding tiding = mTidings.get(position);
        holder.titleTextField.setText(tiding.getTitle());
        holder.publicationDateTextField.setText(getDate(tiding.getPublicationDate()));
        holder.itemView.setOnClickListener(__ -> mItemListener.onTidingClick(position));
        if (tiding.isFavorite()) {
            holder.imageView.setImageResource(R.drawable.ic_favorite_red);
        } else {
            holder.imageView.setImageResource(R.drawable.ic_favorite_border_grey);
        }

        holder.imageView.setOnClickListener(__ -> {
                mItemListener.onLikeClick(tiding, !tiding.isFavorite());
        });
    }


    @Override
    public int getItemCount() {
        return mTidings.size();
    }

    class TidingViewHolder extends RecyclerView.ViewHolder {

        TextView titleTextField;

        ImageView imageView;
        TextView publicationDateTextField;

        TidingViewHolder(View itemView) {
            super(itemView);
            titleTextField = (TextView) itemView.findViewById(R.id.title);
            imageView = (ImageView) itemView.findViewById(R.id.favorites_icon);
            publicationDateTextField = (TextView) itemView.findViewById(R.id.publication_date);
        }
    }

    private void setList(List<Tiding> tidings) {
        mTidings = tidings;
    }

    public void replaceData(List<Tiding> tidings) {
        setList(tidings);
        notifyDataSetChanged();
    }

    private String getDate(long timeStamp){

        try{
            DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }
}
