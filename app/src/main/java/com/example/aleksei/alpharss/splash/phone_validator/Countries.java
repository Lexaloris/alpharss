package com.example.aleksei.alpharss.splash.phone_validator;

import java.util.ArrayList;
import java.util.List;

public class Countries {
    public static final List<Country> COUNTRIES = new ArrayList<>();
    static {
        COUNTRIES.add(new Country("ru", "Russia (Россия)", 7));
        COUNTRIES.add(new Country("by", "Belarus (Беларусь)", 375));
        COUNTRIES.add(new Country("ua", "Ukraine (Україна)", 380));
        COUNTRIES.add(new Country("az", "Azerbaijan (Azərbaycan)", 994));
        COUNTRIES.add(new Country("uz", "Uzbekistan (Oʻzbekiston)", 998));
    }
}
