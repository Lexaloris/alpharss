package com.example.aleksei.alpharss.tiding_pager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.aleksei.alpharss.Injection;
import com.example.aleksei.alpharss.R;
import com.example.aleksei.alpharss.data.Tiding;
import com.example.aleksei.alpharss.tiding_pager.tiding_detail.TidingDetailFragment;
import com.example.aleksei.alpharss.tiding_pager.tiding_detail.TidingDetailPresenter;
import com.example.aleksei.alpharss.tidings.TidingsListPresenter;
import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.plus.PlusShare;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import com.vk.sdk.dialogs.VKShareDialog;
import com.vk.sdk.dialogs.VKShareDialogBuilder;

import java.util.List;

import static com.example.aleksei.alpharss.MainActivity.CURRENT_FILTERING_KEY;

public class TidingPagerActivity extends AppCompatActivity implements
        TidingPagerContract.View, TidingPagerContract.SharingView {

    public static final String EXTRA_TIDING_POSITION = "TIDING_POSITION";
    private static final int GOOGLEPLUS_REQUEST_CODE = 1001;

    private static String vkTokenKey = "VK_ACCESS_TOKEN";
    private static String[] vkScope = new String[]{VKScope.WALL};

    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    private TidingPagerContract.Presenter mPresenter;

    private ViewPager pager;

    private MyFragmentPagerAdapter pagerAdapter;

    private int tidingPosition;

    private MenuItem mFavoritesMenuItem;

    private List<Tiding> mTidings;

    private TidingsListPresenter.TidingFilterType mFilterType;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tiding_detail_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowHomeEnabled(true);

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setOffscreenPageLimit(2);

        tidingPosition = getIntent().getIntExtra(EXTRA_TIDING_POSITION, 0);
        mFilterType = TidingsListPresenter.TidingFilterType.valueOf(getIntent().getStringExtra(CURRENT_FILTERING_KEY));

        mPresenter = new TidingPagerPresenter(
                Injection.provideTasksRepository(getApplicationContext()),
                this,
                Injection.provideSchedulerProvider());

        mPresenter.setFiltering(mFilterType);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                invalidateMenu(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void setPresenter(TidingPagerContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showTidings(List<Tiding> tidings) {
        mTidings = tidings;
        pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager(), tidings);
        pager.setAdapter(pagerAdapter);
        pager.setCurrentItem(tidingPosition);
    }

    private class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {

        List<Tiding> mTidings;

        public MyFragmentPagerAdapter(FragmentManager supportFragmentManager, List<Tiding> tidings) {
            super(supportFragmentManager);
            mTidings = tidings;
        }

        @Override
        public Fragment getItem(int position) {

            TidingDetailFragment taskDetailFragment = TidingDetailFragment.newInstance(mTidings.get(position).getId());

            new TidingDetailPresenter(
                    mTidings.get(position).getId(),
                    Injection.provideTasksRepository(getApplicationContext()),
                    taskDetailFragment,
                    Injection.provideSchedulerProvider());

            return taskDetailFragment;
        }

        public Tiding getTiding(int position) {
            return mTidings.get(position);
        }

        @Override
        public int getCount() {
            return mTidings.size();
        }

        private void setList(List<Tiding> tidings) {
            mTidings = tidings;
        }

        public void replaceData(List<Tiding> tidings) {
            setList(tidings);
            notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tiding_menu, menu);
        mFavoritesMenuItem = menu.getItem(0);
        invalidateMenu(tidingPosition);
        return true;
    }

    public Tiding getCurrentTiding(int position) {
        return pagerAdapter.getTiding(position);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_favorites:
                mPresenter.like(pager.getCurrentItem());
                break;
            case R.id.menu_share:
                showSharePopUpMenu();
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void showSharePopUpMenu() {
        PopupMenu popup = new PopupMenu(this, findViewById(R.id.menu_share));
        popup.getMenuInflater().inflate(R.menu.share_tidings, popup.getMenu());

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.google_plus:
                    mPresenter.share(pager.getCurrentItem(), TidingPagerContract.Presenter.SHARE_TYPE.GOOGLE_PLUS);
                    break;
                case R.id.facebook:
                    mPresenter.share(pager.getCurrentItem(), TidingPagerContract.Presenter.SHARE_TYPE.FACEBOOK);
                    break;
                case R.id.vkontakte:
                    mPresenter.share(pager.getCurrentItem(), TidingPagerContract.Presenter.SHARE_TYPE.VKONTAKTE);
                    break;
                default:
                    mPresenter.share(pager.getCurrentItem(), TidingPagerContract.Presenter.SHARE_TYPE.ALL_APPLICATIONS);
                    break;
            }
            return true;
        });

        popup.show();
    }


    @Override
    public void invalidateMenu(int position) {
        if (pagerAdapter == null) {
            return;
        }
        Tiding curTiding = getCurrentTiding(position);
        if (curTiding.isFavorite()) {
            mFavoritesMenuItem.setIcon(R.drawable.ic_favorite_red);
        } else {
            mFavoritesMenuItem.setIcon(R.drawable.ic_favorite_border_grey);
        }
    }

    @Override
    public void update(int position, Tiding tiding) {
        mTidings.remove(position);
        mTidings.add(position, tiding);
        pagerAdapter.replaceData(mTidings);
    }

    @Override
    public void shareFacebook(Tiding tiding) {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse(tiding.getLink()))
                    .build();
            shareDialog.show(content);
        }
    }

    @Override
    public void shareAllApplications(Tiding tiding) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, tiding.getTitle() + "\n" + tiding.getLink());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    public void shareVkontakte(Tiding tiding) {
        VKAccessToken token = VKAccessToken.tokenFromSharedPreferences(this, vkTokenKey);
        if ((token == null) || token.isExpired()) {
            VKSdk.login(this, vkScope);
            showMessage(getString(R.string.vk_login_need));
        } else {
            VKShareDialogBuilder builder = new VKShareDialogBuilder();
            builder.setAttachmentLink(tiding.getTitle(), tiding.getLink());
            builder.setShareDialogListener(new VKShareDialog.VKShareDialogListener() {
                @Override
                public void onVkShareComplete(int postId) {

                }

                @Override
                public void onVkShareCancel() {

                }

                @Override
                public void onVkShareError(VKError error) {

                }
            });
            builder.show(getFragmentManager(), "VK_SHARE_DIALOG");
        }
    }

    @Override
    public void shareGooglePlus(Tiding tiding) {
        Intent shareIntent = new PlusShare.Builder(this)
                .setType("text/plain")
                .setText(tiding.getTitle())
                .setContentUrl(Uri.parse(tiding.getLink()))
                .getIntent();
        startActivityForResult(shareIntent, GOOGLEPLUS_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == GOOGLEPLUS_REQUEST_CODE) && (resultCode == -1)) {
            //Do something if success
        }

        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                res.saveTokenToSharedPreferences(getApplicationContext(), vkTokenKey);
            }
            @Override
            public void onError(VKError error) {

            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void showMessage(String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToolbarTitle() {
        switch (mFilterType) {
            case FAVORITES_TIDINGS:
                toolbar.setTitle(getResources().getString(R.string.label_favorites_tidings));
                break;
            default:
                toolbar.setTitle(getResources().getString(R.string.label_all_tidings));
        }
    }

}
