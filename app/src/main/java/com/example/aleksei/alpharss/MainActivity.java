package com.example.aleksei.alpharss;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.aleksei.alpharss.background.AlarmActivity;
import com.example.aleksei.alpharss.background.MyAlarmService;
import com.example.aleksei.alpharss.tidings.TidingsListContract;
import com.example.aleksei.alpharss.tidings.TidingsListFragment;
import com.example.aleksei.alpharss.tidings.TidingsListPresenter;
import com.example.aleksei.alpharss.utils.ActivityUtils;

public class MainActivity extends AlarmActivity implements TidingsListContract.TitleView {

    public static final String CURRENT_FILTERING_KEY = "CURRENT_FILTERING_KEY";

    private TidingsListPresenter mPresenter;

    private DrawerLayout mDrawerLayout;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.ic_menu);
            ab.setDisplayHomeAsUpEnabled(true);
        }

        // Set up the navigation drawer.
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setStatusBarBackground(R.color.colorPrimaryDark);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        TidingsListFragment tidingListFragment =
                (TidingsListFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);

        if (tidingListFragment == null) {
            tidingListFragment = TidingsListFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), tidingListFragment, R.id.contentFrame);
        }

        mPresenter = new TidingsListPresenter(
                Injection.provideTasksRepository(getApplicationContext()),
                tidingListFragment,
                Injection.provideSchedulerProvider());

        // Load previously saved state, if available.
        if (savedInstanceState != null) {
            TidingsListPresenter.TidingFilterType currentFiltering =
                    (TidingsListPresenter.TidingFilterType) savedInstanceState.getSerializable(CURRENT_FILTERING_KEY);
            if (currentFiltering != null) {
                mPresenter.setFiltering(currentFiltering);
            }
        }
        cancelAlarm();
        scheduleAlarm();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(CURRENT_FILTERING_KEY, mPresenter.getFiltering());

        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Open the navigation drawer when the home icon is selected from the toolbar.
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    switch (menuItem.getItemId()) {
                        case R.id.list_navigation_menu_item:
                            // Do nothing, we're already on that screen
                            break;
                        case R.id.profile_navigation_menu_item:
                            mPresenter.processToProfile();
                            break;
                        case R.id.exit_navigation_menu_item:
                            cancelAlarm();
                            mPresenter.processToSplash();
                            break;
                        default:
                            break;
                    }
                    // Close the navigation drawer when an item is selected.
                    menuItem.setChecked(true);
                    mDrawerLayout.closeDrawers();
                    return true;
                });
    }

    @Override
    public void setToolbarTitle(String title) {
        toolbar.setTitle(title);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(MyAlarmService.ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(testReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(testReceiver);
    }

    private BroadcastReceiver testReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int resultCode = intent.getIntExtra("resultCode", RESULT_CANCELED);
            if (resultCode == RESULT_OK) {
                mPresenter.loadTidings(false);
            }
        }
    };
}
