package com.example.aleksei.alpharss.tiding_pager;

import com.example.aleksei.alpharss.BasePresenter;
import com.example.aleksei.alpharss.BaseView;
import com.example.aleksei.alpharss.data.Tiding;
import com.example.aleksei.alpharss.tidings.TidingsListPresenter;

import java.util.List;

public interface TidingPagerContract {

    interface View extends BaseView<Presenter> {

        void showTidings(List<Tiding> tidings);

        void invalidateMenu(int position);

        Tiding getCurrentTiding(int position);

        void update(int position, Tiding tiding);

        void showSharePopUpMenu();

        void showToolbarTitle();
    }

    interface SharingView {

        void shareFacebook(Tiding tiding);

        void shareAllApplications(Tiding tiding);

        void shareVkontakte(Tiding tiding);

        void shareGooglePlus(Tiding tiding);
    }

    interface Presenter extends BasePresenter {

        enum SHARE_TYPE {
            FACEBOOK,
            GOOGLE_PLUS,
            VKONTAKTE,
            ALL_APPLICATIONS
        }

        void share(int position, SHARE_TYPE shareType);

        void like(int position);

        void setFiltering(TidingsListPresenter.TidingFilterType requestType);
    }
}
