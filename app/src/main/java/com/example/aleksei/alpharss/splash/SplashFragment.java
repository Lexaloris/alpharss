package com.example.aleksei.alpharss.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.aleksei.alpharss.MainActivity;
import com.example.aleksei.alpharss.R;
import com.example.aleksei.alpharss.splash.phone_validator.Countries;
import com.example.aleksei.alpharss.splash.phone_validator.PhoneInputLayout;
import com.example.aleksei.alpharss.utils.InternetAvailabilityChecker;

public class SplashFragment extends Fragment implements SplashContract.View {

    private View root;

    private SplashContract.Presenter mPresenter;

    private SwipeRefreshLayout srl;

    private Button mEnterButton;

    private LinearLayout phoneField;

    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.splash_fragment, container, false);

        srl = (SwipeRefreshLayout) root.findViewById(R.id.refresh_layout);
        srl.setRefreshing(false);
        srl.setEnabled(false);


        final PhoneInputLayout phoneInputLayout =
                (PhoneInputLayout) root.findViewById(R.id.phone_input_layout);

        assert phoneInputLayout != null;

        phoneInputLayout.setHint(R.string.phone_hint);

        if (Countries.COUNTRIES.size() > 0) {
            phoneInputLayout.setDefaultCountry(Countries.COUNTRIES.get(0).getCode());
        }

        phoneField = (LinearLayout) root.findViewById(R.id.phone_field);
        mEnterButton = (Button) root.findViewById(R.id.enterButton);
        mEnterButton.setOnClickListener(v -> {
            if (phoneInputLayout.isValid()) {
                phoneInputLayout.setError(null);
                mPresenter.loadTidings(true, true);
            } else {
                phoneInputLayout.setError(getString(R.string.invalid_phone_number));
            }
        });

        return root;
    }

    @Override
    public void setPresenter(SplashContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (getView() == null) {
            return;
        }
        srl.post(() -> srl.setRefreshing(active));
    }

    @Override
    public void showLoadingTidingsError() {
        showMessage(getString(R.string.loading_tidings_error));
    }

    @Override
    public void showMainScreen() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @Override
    public void showEnterForms() {
        phoneField.setVisibility(View.VISIBLE);
        mEnterButton.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean isInternetAvailable() {
        InternetAvailabilityChecker checker = new InternetAvailabilityChecker();
        return checker.isInternetAvailable(getContext());
    }

    @Override
    public void showNoInternet() {
        showMessage(getString(R.string.no_internet_error));
    }

    private void showMessage(String message) {
        Snackbar.make(root, message, Snackbar.LENGTH_SHORT).show();
    }
}
