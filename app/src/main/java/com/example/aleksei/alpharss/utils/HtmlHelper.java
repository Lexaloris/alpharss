package com.example.aleksei.alpharss.utils;

import com.example.aleksei.alpharss.data.source.remote.network.NetworkClient;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class HtmlHelper {

    public String fullHtmlWithCss(String response, String url, NetworkClient client) throws IOException {

        //cssToGrab - list of all css files to download and parse, these need to be parsed to extract urls
        Set<String> cssToGrab = new HashSet<>();
        Document document = Jsoup.parse(response, url);
        Elements links;
        links = document.select("link[href]");
        for (Element link : links) {
            //if it is css, parse it later to extract urls (images referenced from "background" attributes for example)
            if (link.attr("rel").equals("stylesheet")) {
                cssToGrab.add(link.attr("abs:href"));
            }
        }
        ArrayList<String> cssList = new ArrayList<>();
        for (String cssUrl : cssToGrab) {
            String cssContent = client.run(cssUrl);
            cssList.add(cssContent);
        }
        Element head = document.body();
        StringBuilder start = new StringBuilder("<style type=\"text/css\">");
        for (String cssItem : cssList) {
            start.append(cssItem);
        }
        start.append("</style>");
        head.append(start.toString());
        return document.html();
    }
}
