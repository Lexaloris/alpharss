package com.example.aleksei.alpharss.data.source.remote.network;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class NetworkClient {

    private OkHttpClient client = new OkHttpClient();

    public String run(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        try (okhttp3.Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }
}
