package com.example.aleksei.alpharss.tiding_pager.tiding_detail;

import com.example.aleksei.alpharss.BasePresenter;
import com.example.aleksei.alpharss.BaseView;

public interface TidingDetailContract {

    interface View extends BaseView<Presenter> {

        void showMissingTiding();

        void setLoadingIndicator(boolean b);

        boolean isInternetAvailable();

        void showLoadingTidingsError();

        void showSource(String url, String source);

        void showNoInternet();
    }

    interface Presenter extends BasePresenter {

        void refreshTidings();
    }
}
