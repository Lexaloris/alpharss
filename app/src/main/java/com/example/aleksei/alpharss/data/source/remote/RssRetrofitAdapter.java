package com.example.aleksei.alpharss.data.source.remote;

import com.example.aleksei.alpharss.data.source.remote.parser.RssAlphaFeed;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RssRetrofitAdapter {
    String RSS_ALPHA_LINK = "https://alfabank.ru/";
    @GET("/_/rss/_rss.html?subtype=1&category=2&city=22")
    Call<RssAlphaFeed> getItems();
}
