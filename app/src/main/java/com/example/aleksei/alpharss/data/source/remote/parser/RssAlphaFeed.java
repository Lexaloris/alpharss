package com.example.aleksei.alpharss.data.source.remote.parser;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "rss", strict = false)
public class RssAlphaFeed {

    @Attribute
    protected
    String version;

    @Element
    private
    RssAlphaChannel channel;

    public RssAlphaChannel getChannel() {
        return channel;
    }

    public void setChannel(RssAlphaChannel channel) {
        this.channel = channel;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "RSS{" +
                "version='" + version + '\'' +
                ", channel=" + channel +
                '}';
    }
}
