package com.example.aleksei.alpharss.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Arrays;
import java.util.UUID;

public class Tiding {

    @NonNull
    private final String mId;

    @Nullable
    private String mTitle;

    private long mPublicationDate;

    @Nullable
    private String mDescription;

    @Nullable
    private String mLink;

    private boolean mIsFavorites;

    public Tiding(@NonNull String itemId, @Nullable String title, long publicationDate,
                  @Nullable String description, @Nullable String link, boolean isFavorites) {
        mId = itemId;
        mTitle = title;
        mPublicationDate = publicationDate;
        mDescription = description;
        mLink = link;
        mIsFavorites = isFavorites;
    }

    @NonNull
    public String getId() {
        return mId;
    }

    @Nullable
    public String getLink() { return mLink;}

    public long getPublicationDate() { return mPublicationDate;}

    @Nullable
    public String getTitle() {
        return mTitle;
    }

    @Nullable
    public String getDescription() {
        return mDescription;
    }

    public boolean isFavorite() {
        return mIsFavorites;
    }

    public Tiding(@Nullable String title, long publicationData,
                  @Nullable  String description,@Nullable String link, boolean isFavorites) {
        mId = UUID.randomUUID().toString();
        mTitle = title;
        mPublicationDate = publicationData;
        mDescription = description;
        mLink = link;
        mIsFavorites = isFavorites;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tiding tiding = (Tiding) o;
        return mId.equals(tiding.getId())
                && (mTitle != null && mTitle.equals(tiding.getTitle())
                && (mDescription != null && mDescription.equals(tiding.getTitle())
                && (mPublicationDate == tiding.getPublicationDate())
                && (mLink != null && mLink.equals(tiding.getTitle())
        )));
    }

    @Override
    public int hashCode() {
        Object[] a = new Object[] {mId, mTitle, mDescription, mPublicationDate, mLink};
        return Arrays.deepHashCode(a);
    }

    public boolean isEmpty() {
        return mTitle != null && !mTitle.isEmpty() && mDescription != null && !mDescription.isEmpty();
    }

    @Override
    public String toString() {
        return "Task with title " + mTitle;
    }

    public void setFavorite(boolean favorite) {
        mIsFavorites = favorite;
    }
}
