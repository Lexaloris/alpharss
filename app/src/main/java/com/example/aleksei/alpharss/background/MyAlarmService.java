package com.example.aleksei.alpharss.background;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;

import com.example.aleksei.alpharss.Injection;
import com.example.aleksei.alpharss.data.Tiding;
import com.example.aleksei.alpharss.data.source.TidingRepository;
import com.example.aleksei.alpharss.utils.schedulers.BaseSchedulerProvider;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class MyAlarmService extends IntentService {

    public static final String ACTION = "com.example.aleksei.alpharss.MyAlarmService";

    @NonNull
    private final TidingRepository mTidingsRepository;

    @NonNull
    private final BaseSchedulerProvider mSchedulerProvider;

    @NonNull
    private CompositeSubscription mSubscriptions;

    public MyAlarmService() {
        super("alarm-service");
        mTidingsRepository = Injection.provideTasksRepository(this);
        mSchedulerProvider = Injection.provideSchedulerProvider();
        mSubscriptions = new CompositeSubscription();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        mTidingsRepository.refreshTidings();

        mSubscriptions.clear();
        Subscription subscription = mTidingsRepository
                .getTidings()
                .flatMap(Observable::from)
                .toList()
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(new Subscriber<List<Tiding>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Tiding> tidings) {

                        Intent in = new Intent(ACTION);
                        in.putExtra("resultCode", Activity.RESULT_OK);
                        LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(in);
                    }
                });

        mSubscriptions.add(subscription);
    }
}
