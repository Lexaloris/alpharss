package com.example.aleksei.alpharss.profile;


import android.support.annotation.NonNull;

import com.example.aleksei.alpharss.data.source.TidingRepository;
import com.example.aleksei.alpharss.utils.schedulers.BaseSchedulerProvider;

import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class ProfilePresenter implements ProfileContract.Presenter {

    @NonNull
    private final TidingRepository mTidingsRepository;

    @NonNull
    private final ProfileContract.View mProfileView;

    @NonNull
    private final BaseSchedulerProvider mSchedulerProvider;

    @NonNull
    private CompositeSubscription mSubscriptions;

    public ProfilePresenter(@NonNull TidingRepository tidingRepository,
                               @NonNull ProfileContract.View profileView,
                               @NonNull BaseSchedulerProvider schedulerProvider) {
        mTidingsRepository = tidingRepository;
        mProfileView = profileView;
        mSchedulerProvider = schedulerProvider;

        mSubscriptions = new CompositeSubscription();
        mProfileView.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void processToSplash() {
        mSubscriptions.clear();
        Subscription subscription = mTidingsRepository
                .clearAll()
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mProfileView.showToSplashError();
                    }

                    @Override
                    public void onNext(Object object) {
                        mProfileView.toSplashScreen();
                    }
                });
        mSubscriptions.add(subscription);
    }

    @Override
    public void processToTidings() {
        mProfileView.toTidingsScreen();
    }
}
