package com.example.aleksei.alpharss.tiding_pager;

import android.support.annotation.NonNull;

import com.example.aleksei.alpharss.data.Tiding;
import com.example.aleksei.alpharss.data.source.TidingRepository;
import com.example.aleksei.alpharss.tidings.TidingsListPresenter;
import com.example.aleksei.alpharss.utils.schedulers.BaseSchedulerProvider;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class TidingPagerPresenter implements TidingPagerContract.Presenter {

    @NonNull
    private final TidingRepository mTidingsRepository;

    @NonNull
    private final TidingPagerContract.View mPagerView;

    @NonNull
    private final BaseSchedulerProvider mSchedulerProvider;

    @NonNull
    private CompositeSubscription mSubscriptions;

    @NonNull
    private TidingsListPresenter.TidingFilterType mCurrentFiltering = TidingsListPresenter.TidingFilterType.ALL_TIDINGS;

    private boolean mFirstLoad = true;

    public TidingPagerPresenter(@NonNull TidingRepository tidingRepository,
                                @NonNull TidingPagerActivity pagerView,
                                @NonNull BaseSchedulerProvider schedulerProvider) {
        mTidingsRepository = tidingRepository;
        mPagerView = pagerView;
        mSchedulerProvider = schedulerProvider;
        mSubscriptions = new CompositeSubscription();
        mPagerView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        if (mFirstLoad) {
            loadTidings();
            mPagerView.showToolbarTitle();
            mFirstLoad = false;
        }
    }

    @Override
    public void unsubscribe() {
        mSubscriptions.clear();
    }

    private void loadTidings() {
        mSubscriptions.clear();
        Subscription subscription = mTidingsRepository
                .getTidings()
                .flatMap(Observable::from)
                .filter(task -> {
                    switch (mCurrentFiltering) {
                        case FAVORITES_TIDINGS:
                            return task.isFavorite();
                        case ALL_TIDINGS:
                        default:
                            return true;
                    }
                })
                .toList()
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(new Subscriber<List<Tiding>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        //mPagerView.showLoadingTidingsError();
                    }

                    @Override
                    public void onNext(List<Tiding> tidings) {
                        processTidings(tidings);
                    }
                });

        mSubscriptions.add(subscription);
    }

    private void processTidings(List<Tiding> tidings) {
        mPagerView.showTidings(tidings);
    }

    @Override
    public void share(int position, SHARE_TYPE shareType) {
        Tiding tiding = mPagerView.getCurrentTiding(position);
        TidingPagerContract.SharingView sharingView = (TidingPagerContract.SharingView) mPagerView;
        switch (shareType) {
            case GOOGLE_PLUS:
                sharingView.shareGooglePlus(tiding);
                break;
            case FACEBOOK:
                sharingView.shareFacebook(tiding);
                break;
            case VKONTAKTE:
                sharingView.shareVkontakte(tiding);
                break;
            default:
                sharingView.shareAllApplications(tiding);
        }
    }

    @Override
    public void like(int position) {
        Tiding tiding = mPagerView.getCurrentTiding(position);

        boolean oppositeLike = !tiding.isFavorite();
        mTidingsRepository.likeTiding(tiding, oppositeLike);
        tiding.setFavorite(oppositeLike);

        mPagerView.update(position, tiding);
        mPagerView.invalidateMenu(position);
    }

    @Override
    public void setFiltering(TidingsListPresenter.TidingFilterType requestType) {
        mCurrentFiltering = requestType;
    }
}
