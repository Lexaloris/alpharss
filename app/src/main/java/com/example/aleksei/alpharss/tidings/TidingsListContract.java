package com.example.aleksei.alpharss.tidings;

import com.example.aleksei.alpharss.BasePresenter;
import com.example.aleksei.alpharss.BaseView;
import com.example.aleksei.alpharss.data.Tiding;

import java.util.List;

public interface TidingsListContract {

    interface View extends BaseView<Presenter> {

        void setLoadingIndicator(boolean active);

        void showNoTidings();

        void showLoadingTidingsError();

        void showTidings(List<Tiding> tidings);

        void showNoInternet();

        boolean isInternetAvailable();

        void showTidingDetailsUi(int position);

        void showFilteringPopUpMenu();

        void showNoFavoriteTidings();

        void showFavoritesFilterLabel();

        void showAllFilterLabel();

        void toSplashScreen();

        void toProfileScreen();

        void showToSplashError();
    }

    interface Presenter extends BasePresenter {

        void loadTidings(boolean forceUpdate);

        void openTidingDetails(int position);

        void setFiltering(TidingsListPresenter.TidingFilterType requestType);

        TidingsListPresenter.TidingFilterType getFiltering();

        void like(Tiding tiding, boolean value);

        void processToSplash();

        void processToProfile();
    }

    interface TitleView {
        void setToolbarTitle(String title);
    }
}
