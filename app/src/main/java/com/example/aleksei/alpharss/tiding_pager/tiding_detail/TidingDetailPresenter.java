package com.example.aleksei.alpharss.tiding_pager.tiding_detail;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.aleksei.alpharss.data.Page;
import com.example.aleksei.alpharss.data.Tiding;
import com.example.aleksei.alpharss.data.source.TidingRepository;
import com.example.aleksei.alpharss.utils.schedulers.BaseSchedulerProvider;

import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class TidingDetailPresenter implements TidingDetailContract.Presenter {

    @NonNull
    private final TidingRepository mTasksRepository;

    @NonNull
    private final TidingDetailContract.View mTidingDetailView;

    @NonNull
    private final BaseSchedulerProvider mSchedulerProvider;

    @Nullable
    private String mTidingId;

    @Nullable
    private Tiding mTiding;

    @NonNull
    private CompositeSubscription mSubscriptions;

    private boolean mFirstLoad = true;

    public TidingDetailPresenter(@Nullable String tidingId,
                                 @NonNull TidingRepository tidingRepository,
                                 @NonNull TidingDetailFragment tidingDetailView,
                                 @NonNull BaseSchedulerProvider baseSchedulerProvider) {
        mTidingId = tidingId;
        mTasksRepository = tidingRepository;
        mTidingDetailView = tidingDetailView;
        mSchedulerProvider = baseSchedulerProvider;

        mSubscriptions = new CompositeSubscription();
        mTidingDetailView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        if (mFirstLoad) {
            loadTidingById();
            mFirstLoad = false;
        }
    }

    @Override
    public void unsubscribe() {
        mSubscriptions.clear();
    }

    private void loadTidingById() {
        if (mTidingId == null) {
            mTidingDetailView.showMissingTiding();
            return;
        }

        mSubscriptions.clear();
        mTidingDetailView.setLoadingIndicator(true);

        Subscription subscription = mTasksRepository
                .getTiding(mTidingId)
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(new Subscriber<Tiding>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mTidingDetailView.showLoadingTidingsError();
                        mTidingDetailView.setLoadingIndicator(false);
                    }

                    @Override
                    public void onNext(Tiding tiding) {
                        mTiding = tiding;
                        loadTiding(tiding.getLink(), false);
                    }
                });

        mSubscriptions.add(subscription);

    }

    private void loadTiding(String url, boolean forceLoad) {
        mSubscriptions.clear();
        mTidingDetailView.setLoadingIndicator(true);

        Subscription subscription = mTasksRepository
                .getPage(url, forceLoad)
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(new Subscriber<Page>() {
                    @Override
                    public void onCompleted() {
                        mTidingDetailView.setLoadingIndicator(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mTidingDetailView.showLoadingTidingsError();
                        mTidingDetailView.setLoadingIndicator(false);
                    }

                    @Override
                    public void onNext(Page page) {
                        if (page == null) {
                            loadTiding(url, true);
                        } else {
                            showTiding(page.getSource());
                        }
                    }
                });

        mSubscriptions.add(subscription);
    }

    private void showTiding(@NonNull String html) {
        mTidingDetailView.showSource(mTiding.getLink(), html);
    }

    @Override
    public void refreshTidings() {
        if (!mTidingDetailView.isInternetAvailable()) {
            mTidingDetailView.showNoInternet();
            mTidingDetailView.setLoadingIndicator(false);
        } else {
            loadTidingById();
        }
    }
}
