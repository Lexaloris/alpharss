package com.example.aleksei.alpharss.splash;


import android.support.annotation.NonNull;

import com.example.aleksei.alpharss.data.Tiding;
import com.example.aleksei.alpharss.data.source.TidingRepository;
import com.example.aleksei.alpharss.utils.schedulers.BaseSchedulerProvider;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class SplashPresenter implements SplashContract.Presenter {

    @NonNull
    private final TidingRepository mTidingsRepository;

    @NonNull
    private final SplashContract.View mSplashView;

    @NonNull
    private final BaseSchedulerProvider mSchedulerProvider;

    @NonNull
    private CompositeSubscription mSubscriptions;


    public SplashPresenter(@NonNull TidingRepository tidingRepository,
                           @NonNull SplashFragment splashFragment,
                           @NonNull BaseSchedulerProvider baseSchedulerProvider) {
        mTidingsRepository = tidingRepository;
        mSplashView = splashFragment;
        mSchedulerProvider = baseSchedulerProvider;
        mSubscriptions = new CompositeSubscription();
        mSplashView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        loadTidings(false, false);
    }

    @Override
    public void unsubscribe() {
        mSubscriptions.clear();
    }

    @Override
    public void loadTidings(boolean forceUpdate, final boolean showLoadingUI) {

        if (forceUpdate  && !mSplashView.isInternetAvailable()) {
            mSplashView.showNoInternet();
            return;
        }

        if (showLoadingUI) {
            mSplashView.setLoadingIndicator(true);
        }
        if (forceUpdate) {
            mTidingsRepository.refreshTidings();
        }

        mSubscriptions.clear();
        Subscription subscription = mTidingsRepository
                .getTidings()
                .flatMap(Observable::from)
                .toList()
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(new Subscriber<List<Tiding>>() {
                    @Override
                    public void onCompleted() {
                        if (!forceUpdate) {
                            mSplashView.setLoadingIndicator(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mSplashView.showLoadingTidingsError();
                    }

                    @Override
                    public void onNext(List<Tiding> tidings) {
                        if (!forceUpdate && tidings.size() > 0 || forceUpdate) {
                            showMainScreen();
                        } else {
                            mSplashView.showEnterForms();
                        }
                    }
                });

        mSubscriptions.add(subscription);
    }

    private void showMainScreen() {
        mSplashView.showMainScreen();
    }

}
