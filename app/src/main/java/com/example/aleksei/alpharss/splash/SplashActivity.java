package com.example.aleksei.alpharss.splash;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.aleksei.alpharss.Injection;
import com.example.aleksei.alpharss.R;
import com.example.aleksei.alpharss.utils.ActivityUtils;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        SplashFragment splashFragment =
                (SplashFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);

        if (splashFragment == null) {
            splashFragment = SplashFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), splashFragment, R.id.contentFrame);
        }

        SplashPresenter mPresenter = new SplashPresenter(
                Injection.provideTasksRepository(getApplicationContext()),
                splashFragment,
                Injection.provideSchedulerProvider());

    }
}
