package com.example.aleksei.alpharss.profile;

import com.example.aleksei.alpharss.BasePresenter;
import com.example.aleksei.alpharss.BaseView;

public interface ProfileContract {

    interface View extends BaseView<Presenter> {

        void toSplashScreen();

        void toTidingsScreen();

        void showToSplashError();
    }

    interface Presenter extends BasePresenter {

        void processToSplash();

        void processToTidings();
    }
}
