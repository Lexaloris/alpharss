package com.example.aleksei.alpharss.data.source;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.example.aleksei.alpharss.data.Page;
import com.example.aleksei.alpharss.data.Tiding;
import com.example.aleksei.alpharss.utils.schedulers.BaseSchedulerProvider;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import rx.Completable;
import rx.Observable;

public class TidingRepository implements TidingDataSource {

    @Nullable
    private static TidingRepository INSTANCE = null;

    @NonNull
    private final TidingDataSource mLocalDataSource;

    @NonNull
    private final TidingDataSource mRemoteDataSource;

    @NonNull
    private final BaseSchedulerProvider schedulerProvider;

    @Nullable
    private Map<String, Page> mCachedPages;

    @VisibleForTesting
    private boolean mCacheIsDirty = false;

    @VisibleForTesting
    private boolean mPagesCacheIsDirty = true;

    private TidingRepository(@NonNull TidingDataSource remoteDataSource,
                             @NonNull TidingDataSource localDataSource,
                             @NonNull BaseSchedulerProvider baseSchedulerProvider) {
        mRemoteDataSource = remoteDataSource;
        mLocalDataSource = localDataSource;
        schedulerProvider= baseSchedulerProvider;
    }

    public static TidingRepository getInstance(TidingDataSource remoteDataSource,
                                               TidingDataSource localDataSource,
                                               BaseSchedulerProvider baseSchedulerProvider) {
        if (INSTANCE == null) {
            INSTANCE = new TidingRepository(remoteDataSource, localDataSource, baseSchedulerProvider);
        }
        return INSTANCE;
    }

    @Override
    public void saveTiding(@NonNull Tiding tiding) {
        mRemoteDataSource.saveTiding(tiding);
        mLocalDataSource.saveTiding(tiding);
    }

    @Override
    public void deleteAllTidings() {
        mRemoteDataSource.deleteAllTidings();
        mLocalDataSource.deleteAllTidings();
    }

    @Override
    public void likeTiding(Tiding tiding, boolean value) {
        mLocalDataSource.likeTiding(tiding, value);
    }

    @Override
    @NonNull
    public Observable<Object> clearAll() {
        return mLocalDataSource.clearAll();
    }

    public Observable<Page> getPage(String url, boolean forceLoad) {

        if (mCachedPages == null) {
            mCachedPages = new HashMap<>();
        } else if (mCachedPages.containsKey(url)) {
            return Observable.just(mCachedPages.get(url));
        }

        Observable<Page> remotePage = getAndCacheRemotePage(url);

        if (forceLoad) {
            return remotePage;
        } else {
            Observable<Page> localPage = getAndCacheLocalPage(url);
            return Observable.concat(localPage, remotePage)
                    .first();
        }
    }

    @Override
    public void savePage(Page page) {
        mRemoteDataSource.savePage(page);
        mLocalDataSource.savePage(page);

        if (mCachedPages == null) {
            mCachedPages = new LinkedHashMap<>();
        }
        mCachedPages.put(page.getLink(), page);
    }

    @Override
    public Observable<List<Page>> getPages() {
        return null;
    }

    @Override
    public void deleteOldPages() {
        mLocalDataSource.deleteOldPages();
    }

    @Override
    public void deleteAllUnlikedTidings() {

    }

    private Observable<Page> getAndCacheRemotePage(String url) {
        return mRemoteDataSource.getPage(url, false)
                .flatMap(pg -> Observable.just(pg)
                .doOnNext(page -> {
                    refreshCachePage(page);
                    refreshLocalDataSourcePage(page);
                })
                .subscribeOn(schedulerProvider.computation()))
                .doOnCompleted(() -> mPagesCacheIsDirty = false);
    }

    private void refreshLocalDataSourcePage(Page page) {
        mLocalDataSource.savePage(page);
    }

    private void refreshCachePage(Page page) {
        if (mCachedPages == null) {
            mCachedPages = new HashMap<>();
        } else {
            mCachedPages.remove(page.getLink());
        }
        mCachedPages.put(page.getLink(), page);
        mPagesCacheIsDirty = false;
    }

    private Observable<Page> getAndCacheLocalPage(String url) {
        return mLocalDataSource.getPage(url, false)
                .flatMap(Observable::just)
                        .doOnNext(page -> {
                            if (mCachedPages != null && page != null) {
                                mCachedPages.put(page.getLink(), page);
                            }
                        });
    }

    @Override
    public Observable<List<Tiding>> getTidings() {
        Observable<List<Tiding>> remoteTidings = getAndSaveRemoteTidings();

        if (mCacheIsDirty) {
            return remoteTidings;
        } else {
            Observable<List<Tiding>> localTidings = getAndCacheLocalTidings();
            return Observable.concat(localTidings, remoteTidings)
                    .first();
        }
    }

    private Observable<List<Tiding>> getAndCacheLocalTidings() {
        return mLocalDataSource.getTidings()
                .flatMap(tidings -> Observable.from(tidings).toList());
    }

    private Observable<List<Tiding>> getAndSaveRemoteTidings() {
        return mRemoteDataSource.getTidings()
                .flatMap(tidings1 -> Observable.from(tidings1)
                        .toList()
                        .doOnNext(tidings -> {
                            if (tidings.size() > 0) {
                                mCacheIsDirty = false;
                                deleteOldPages();
                                refreshLocalDataSource(tidings);
                            }
                        })
                        .subscribeOn(schedulerProvider.computation()))
                .doOnCompleted(() -> mCacheIsDirty = false);
    }

    private void refreshLocalDataSource(List<Tiding> tidings) {
        mLocalDataSource.deleteAllUnlikedTidings();
        for (Tiding tiding : tidings) {
            mLocalDataSource.saveTiding(tiding);
        }
    }

    @Override
    public void refreshTidings() {
        mCacheIsDirty = true;
    }

    @Override
    public Observable<Tiding> getTiding(@NonNull String tidingId) {
        return getTaskWithIdFromLocalRepository(tidingId);
    }

    @NonNull
    private Observable<Tiding> getTaskWithIdFromLocalRepository(@NonNull final String tidingId) {
        return mLocalDataSource
                .getTiding(tidingId)
                .first();
    }

    public Completable getSourceTiding(String mTidingId) {
        return null;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
