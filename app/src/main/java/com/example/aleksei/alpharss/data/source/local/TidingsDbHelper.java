package com.example.aleksei.alpharss.data.source.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TidingsDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "Tidings.db";

    private static final String TEXT_TYPE = " TEXT";

    private static final String INTEGER_TYPE = " INTEGER";

    private static final String FOREIGN_KEY = "FOREIGN KEY";

    private static final String REFERENCES = "REFERENCES";

    private static final String BOOLEAN_TYPE = " INTEGER";

    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TidingsPersistenceContract.TidingEntry.TABLE_NAME + " (" +
                    TidingsPersistenceContract.TidingEntry._ID + TEXT_TYPE + " PRIMARY KEY," +
                    TidingsPersistenceContract.TidingEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + COMMA_SEP +
                    TidingsPersistenceContract.TidingEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    TidingsPersistenceContract.TidingEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    TidingsPersistenceContract.TidingEntry.COLUMN_NAME_PUBLICATION_DATE + INTEGER_TYPE + COMMA_SEP +
                    TidingsPersistenceContract.TidingEntry.COLUMN_NAME_LINK + TEXT_TYPE + COMMA_SEP +
                    TidingsPersistenceContract.TidingEntry.COLUMN_NAME_FAVORITES + BOOLEAN_TYPE + COMMA_SEP +
                    " UNIQUE( " + TidingsPersistenceContract.TidingEntry.COLUMN_NAME_TITLE + " " + COMMA_SEP + " " +
                    TidingsPersistenceContract.TidingEntry.COLUMN_NAME_LINK + " )" +
                    " )";

    private static final String SQL_CREATE_PAGES =
            "CREATE TABLE " + TidingsPersistenceContract.PageEntry.TABLE_NAME + " (" +
                    TidingsPersistenceContract.PageEntry._ID + TEXT_TYPE + " PRIMARY KEY," +
                    TidingsPersistenceContract.PageEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + COMMA_SEP +
                    TidingsPersistenceContract.PageEntry.COLUMN_NAME_LINK + TEXT_TYPE + COMMA_SEP +
                    TidingsPersistenceContract.PageEntry.COLUMN_NAME_SOURCE + TEXT_TYPE + COMMA_SEP +
                    FOREIGN_KEY + " (" + TidingsPersistenceContract.PageEntry.COLUMN_NAME_LINK + " )" +
                    REFERENCES + " " + TidingsPersistenceContract.TidingEntry.TABLE_NAME +
                    " (" + TidingsPersistenceContract.TidingEntry.COLUMN_NAME_LINK + " )" +
                    " )";


    public TidingsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
        db.execSQL(SQL_CREATE_PAGES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
