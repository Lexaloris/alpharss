package com.example.aleksei.alpharss.data.source.remote;

import android.support.annotation.NonNull;

import com.example.aleksei.alpharss.BuildConfig;
import com.example.aleksei.alpharss.data.Page;
import com.example.aleksei.alpharss.data.Tiding;
import com.example.aleksei.alpharss.data.source.TidingDataSource;
import com.example.aleksei.alpharss.data.source.remote.network.NetworkClient;
import com.example.aleksei.alpharss.data.source.remote.parser.RssAlphaFeed;
import com.example.aleksei.alpharss.data.source.remote.parser.RssAlphaItem;
import com.example.aleksei.alpharss.utils.HtmlHelper;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
import rx.Observable;

import static com.example.aleksei.alpharss.data.source.remote.RssRetrofitAdapter.RSS_ALPHA_LINK;

public class TidingRemoteDataSource implements TidingDataSource {

    private static TidingRemoteDataSource INSTANCE;
    private DateFormat formatter;

    private TidingRemoteDataSource() {
        formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
    }

    public static TidingRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new TidingRemoteDataSource();
        }
        return INSTANCE;
    }

    @Override
    public void saveTiding(Tiding tiding) {

    }

    @Override
    public void deleteAllTidings() {

    }

    @Override
    public Observable<Page> getPage(String url, boolean forceLoad) {
        return Observable.create(subscriber -> {
            NetworkClient client = new NetworkClient();
            try {
                String response = client.run(url);
                HtmlHelper htmlHelper = new HtmlHelper();
                String html = htmlHelper.fullHtmlWithCss(response, url, client);
                subscriber.onNext(new Page(url, html));
                subscriber.onCompleted();
            } catch (IOException e) {
                subscriber.onError(e);
            }
        });
    }

    @Override
    public void savePage(Page page) {

    }

    @Override
    public Observable<List<Page>> getPages() {
        return null;
    }

    @Override
    public void deleteOldPages() {

    }

    @Override
    public void deleteAllUnlikedTidings() {

    }

    @Override
    public void likeTiding(Tiding tiding, boolean value) {

    }

    @Override
    public Observable<Object> clearAll() {
        return null;
    }

    @Override
    public Observable<List<Tiding>> getTidings() {

        return Observable.create(subscriber -> {
            RssRetrofitAdapter retrofitService = buildRetrofit().create(RssRetrofitAdapter.class);
            Call<RssAlphaFeed> call = retrofitService.getItems();
            call.enqueue(new Callback<RssAlphaFeed>() {
                @Override
                public void onResponse(@NonNull Call<RssAlphaFeed> call, @NonNull Response<RssAlphaFeed> response) {
                    RssAlphaFeed feed = response.body();
                    List<RssAlphaItem> mItems = feed != null ? feed.getChannel().getLentaItemList() : null;

                    List<Tiding> tidings = getTidings(mItems);
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onNext(tidings);
                        subscriber.onCompleted();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RssAlphaFeed> call, @NonNull Throwable t) {
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onNext(new ArrayList<>());
                        subscriber.onCompleted();
                    }
                }
            });
        });
    }

    private Retrofit buildRetrofit() {
        SimpleXmlConverterFactory converterFactory = SimpleXmlConverterFactory.createNonStrict();
        return new Retrofit.Builder()
                .baseUrl(RSS_ALPHA_LINK)
                .client(buildClient())
                .addConverterFactory(converterFactory)
                .build();
    }

    private OkHttpClient buildClient() {
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            return new OkHttpClient.Builder().addInterceptor(interceptor).build();
        } else {
            return new OkHttpClient.Builder().build();
        }
    }

    private List<Tiding> getTidings(List<RssAlphaItem> mItems) {
        List<Tiding> tidings = new ArrayList<>();
        for (RssAlphaItem item: mItems) {
            try {
                String title = item.getTitle();
                Date date = formatter.parse(item.getPublicationDate());
                String description = item.getDescription();
                String link = item.getLink();
                tidings.add(new Tiding(title, date.getTime(), description, link, false));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return tidings;
    }

    @Override
    public void refreshTidings() {

    }

    @Override
    public Observable<Tiding> getTiding(@NonNull String tidingId) {
        return null;
    }
}
