package com.example.aleksei.alpharss.tidings;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.aleksei.alpharss.R;
import com.example.aleksei.alpharss.data.Tiding;
import com.example.aleksei.alpharss.profile.ProfileActivity;
import com.example.aleksei.alpharss.splash.SplashActivity;
import com.example.aleksei.alpharss.tiding_pager.TidingPagerActivity;
import com.example.aleksei.alpharss.utils.InternetAvailabilityChecker;

import java.util.ArrayList;
import java.util.List;

import static com.example.aleksei.alpharss.MainActivity.CURRENT_FILTERING_KEY;

public class TidingsListFragment extends Fragment implements TidingsListContract.View {

    private TidingsListContract.Presenter mPresenter;

    private View root;
    private TextView noTidings;
    private Button loadButton;

    private RecyclerView recyclerView;
    TidingListAdapter adapter;

    private SwipeRefreshLayout srl;

    public TidingsListFragment() {

    }

    public static TidingsListFragment newInstance() {
        return new TidingsListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.tiding_list_fragment, container, false);

        noTidings = (TextView) root.findViewById(R.id.no_tidings);
        loadButton = (Button) root.findViewById(R.id.loadButton);

        recyclerView = (RecyclerView) root.findViewById(R.id.tidings_rv);
        recyclerView.addItemDecoration(new MarginDecoration(getContext()));
        LinearLayoutManager layoutMgr = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutMgr);

        srl = (SwipeRefreshLayout) root.findViewById(R.id.refresh_layout);
        final ScrollChildSwipeRefreshLayout swipeRefreshLayout =
                (ScrollChildSwipeRefreshLayout) srl;
        swipeRefreshLayout.setScrollUpChild(recyclerView);

        swipeRefreshLayout.setOnRefreshListener(() -> mPresenter.loadTidings(true));
        loadButton.setOnClickListener(v -> mPresenter.loadTidings(true));

        setHasOptionsMenu(true);

        adapter = new TidingListAdapter(new ArrayList<>(), mItemListener);
        recyclerView.setAdapter(adapter);

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.tiding_list_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_filter:
                showFilteringPopUpMenu();
                break;
        }
        return true;
    }

    @Override
    public void showFilteringPopUpMenu() {
        PopupMenu popup = new PopupMenu(getContext(), getActivity().findViewById(R.id.menu_filter));
        popup.getMenuInflater().inflate(R.menu.filter_tidings, popup.getMenu());

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.favorites:
                    mPresenter.setFiltering(TidingsListPresenter.TidingFilterType.FAVORITES_TIDINGS);

                    break;
                default:
                    mPresenter.setFiltering(TidingsListPresenter.TidingFilterType.ALL_TIDINGS);
                    break;
            }
            mPresenter.loadTidings(false);
            return true;
        });

        popup.show();
    }

    @Override
    public void setPresenter(TidingsListContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (getView() == null) {
            return;
        }
        srl.post(() -> srl.setRefreshing(active));
    }

    @Override
    public void showNoFavoriteTidings() {
        recyclerView.setVisibility(View.GONE);
        noTidings.setVisibility(View.VISIBLE);
        noTidings.setText(getResources().getString(R.string.no_favorites_tidings_text));
        loadButton.setVisibility(View.GONE);
    }

    @Override
    public void showNoTidings() {
        recyclerView.setVisibility(View.GONE);
        noTidings.setVisibility(View.VISIBLE);
        noTidings.setText(getResources().getString(R.string.no_tidings_text));
        loadButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void showFavoritesFilterLabel() {
        getTitleView().setToolbarTitle(getResources().getString(R.string.label_favorites_tidings));
    }

    @Override
    public void showAllFilterLabel() {
        getTitleView().setToolbarTitle(getResources().getString(R.string.label_all_tidings));
    }

    private TidingsListContract.TitleView getTitleView() {
        return (TidingsListContract.TitleView) getActivity();
    }

    @Override
    public void showNoInternet() {
        showMessage(getString(R.string.no_internet_error));
    }

    @Override
    public boolean isInternetAvailable() {
        InternetAvailabilityChecker checker = new InternetAvailabilityChecker();
        return checker.isInternetAvailable(getContext());
    }

    @Override
    public void showTidingDetailsUi(int position) {
        Intent intent = new Intent(getContext(), TidingPagerActivity.class);
        intent.putExtra(TidingPagerActivity.EXTRA_TIDING_POSITION, position);
        intent.putExtra(CURRENT_FILTERING_KEY, mPresenter.getFiltering().toString());
        startActivity(intent);
    }

    @Override
    public void showLoadingTidingsError() {
        showMessage(getString(R.string.loading_tidings_error));
    }

    private void showMessage(String message) {
        Snackbar.make(root, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showTidings(List<Tiding> tidings) {
        adapter.replaceData(tidings);

        recyclerView.setVisibility(View.VISIBLE);
        noTidings.setVisibility(View.GONE);
        loadButton.setVisibility(View.GONE);
    }

    public class MarginDecoration extends RecyclerView.ItemDecoration {
        private int margin;

        MarginDecoration(Context context) {
            margin = context.getResources().getDimensionPixelSize(R.dimen.item_margin);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.set(margin, margin, margin, margin);
        }
    }

    TidingItemListener mItemListener = new TidingItemListener() {
        @Override
        public void onTidingClick(int position) {
            mPresenter.openTidingDetails(position);
        }

        @Override
        public void onLikeClick(Tiding tiding, boolean value) {
            mPresenter.like(tiding, value);
        }
    };

    public interface TidingItemListener {

        void onTidingClick(int position);

        void onLikeClick(Tiding tiding, boolean value);
    }

    @Override
    public void toSplashScreen() {
        Intent splashIntent =
                new Intent(getContext(), SplashActivity.class);
        splashIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(splashIntent);
    }

    @Override
    public void toProfileScreen() {
        Intent profileIntent =
                new Intent(getContext(), ProfileActivity.class);
        profileIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(profileIntent);
    }

    @Override
    public void showToSplashError() {

    }
}
