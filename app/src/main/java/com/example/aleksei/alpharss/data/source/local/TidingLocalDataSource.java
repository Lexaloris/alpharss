package com.example.aleksei.alpharss.data.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.example.aleksei.alpharss.data.Page;
import com.example.aleksei.alpharss.data.Tiding;
import com.example.aleksei.alpharss.data.source.TidingDataSource;
import com.example.aleksei.alpharss.utils.schedulers.BaseSchedulerProvider;
import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.util.List;

import rx.Observable;
import rx.functions.Func1;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;
import static android.database.sqlite.SQLiteDatabase.CONFLICT_REPLACE;

public class TidingLocalDataSource implements TidingDataSource {

    @Nullable
    private static TidingLocalDataSource INSTANCE;

    @NonNull
    private final BriteDatabase mDatabaseHelper;

    @NonNull
    private Func1<Cursor, Tiding> mTidingMapperFunction;

    @NonNull
    private Func1<Cursor, Page> mPageMapperFunction;

    private TidingLocalDataSource(@NonNull Context context, BaseSchedulerProvider schedulerProvider) {
        TidingsDbHelper dbHelper = new TidingsDbHelper(context);
        SqlBrite sqlBrite = SqlBrite.create();
        mDatabaseHelper = sqlBrite.wrapDatabaseHelper(dbHelper, schedulerProvider.computation());
        mTidingMapperFunction = this::getTiding;
        mPageMapperFunction = this::getPage;
    }

    public static TidingLocalDataSource getInstance(
            @NonNull Context context,
            @NonNull BaseSchedulerProvider schedulerProvider) {
        if (INSTANCE == null) {
            INSTANCE = new TidingLocalDataSource(context, schedulerProvider);
        }
        return INSTANCE;
    }

    @Override
    public void savePage(Page page) {
        ContentValues values = new ContentValues();
        values.put(TidingsPersistenceContract.PageEntry.COLUMN_NAME_ENTRY_ID, page.getId());
        values.put(TidingsPersistenceContract.PageEntry.COLUMN_NAME_LINK, page.getLink());
        values.put(TidingsPersistenceContract.PageEntry.COLUMN_NAME_SOURCE, page.getSource());

        mDatabaseHelper.insert(TidingsPersistenceContract.PageEntry.TABLE_NAME, values, CONFLICT_REPLACE);
    }

    private Page getPage(Cursor c) {

        String itemId = c.getString(c.getColumnIndexOrThrow(TidingsPersistenceContract.PageEntry.COLUMN_NAME_ENTRY_ID));
        String link = c.getString(c.getColumnIndexOrThrow(TidingsPersistenceContract.PageEntry.COLUMN_NAME_LINK));
        String source = c.getString(c.getColumnIndexOrThrow(TidingsPersistenceContract.PageEntry.COLUMN_NAME_SOURCE));

        return new Page(itemId, link, source);
    }

    @Override
    public Observable<List<Page>> getPages() {
        String[] projection = {
                TidingsPersistenceContract.PageEntry.COLUMN_NAME_ENTRY_ID,
                TidingsPersistenceContract.PageEntry.COLUMN_NAME_LINK,
                TidingsPersistenceContract.PageEntry.COLUMN_NAME_SOURCE,
        };
        String sql = String.format("SELECT %s FROM %s", TextUtils.join(",", projection),
                TidingsPersistenceContract.PageEntry.TABLE_NAME);
        return mDatabaseHelper.createQuery(TidingsPersistenceContract.PageEntry.TABLE_NAME, sql)
                .mapToList(mPageMapperFunction);
    }

    @Override
    public Observable<Page> getPage(String url, boolean forceLoad) {
        String[] projection = {
                TidingsPersistenceContract.PageEntry.COLUMN_NAME_ENTRY_ID,
                TidingsPersistenceContract.PageEntry.COLUMN_NAME_LINK,
                TidingsPersistenceContract.PageEntry.COLUMN_NAME_SOURCE,
        };
        String sql = String.format("SELECT %s FROM %s WHERE %s LIKE ?",
                TextUtils.join(",", projection), TidingsPersistenceContract.PageEntry.TABLE_NAME,
                TidingsPersistenceContract.PageEntry.COLUMN_NAME_LINK);
        return mDatabaseHelper.createQuery(TidingsPersistenceContract.PageEntry.TABLE_NAME, sql, url)
                .mapToOneOrDefault(mPageMapperFunction, null);
    }

    @Override
    public void saveTiding(Tiding tiding) {

        ContentValues values = new ContentValues();
        values.put(TidingsPersistenceContract.TidingEntry.COLUMN_NAME_ENTRY_ID, tiding.getId());
        values.put(TidingsPersistenceContract.TidingEntry.COLUMN_NAME_TITLE, tiding.getTitle());
        values.put(TidingsPersistenceContract.TidingEntry.COLUMN_NAME_DESCRIPTION, tiding.getDescription());
        values.put(TidingsPersistenceContract.TidingEntry.COLUMN_NAME_PUBLICATION_DATE, tiding.getPublicationDate());
        values.put(TidingsPersistenceContract.TidingEntry.COLUMN_NAME_LINK, tiding.getLink());
        values.put(TidingsPersistenceContract.TidingEntry.COLUMN_NAME_FAVORITES, tiding.isFavorite());

        mDatabaseHelper.insert(TidingsPersistenceContract.TidingEntry.TABLE_NAME, values, CONFLICT_IGNORE);
    }

    @Override
    public void deleteAllTidings() {
        mDatabaseHelper.delete(TidingsPersistenceContract.TidingEntry.TABLE_NAME, null, null);
    }

    @Override
    public void deleteOldPages() {
        String query = "DELETE FROM " + TidingsPersistenceContract.PageEntry.TABLE_NAME + " WHERE " +
                TidingsPersistenceContract.PageEntry.COLUMN_NAME_LINK + " NOT IN " +
                " ( " + " SELECT " + TidingsPersistenceContract.TidingEntry.COLUMN_NAME_LINK +
                " FROM " + TidingsPersistenceContract.TidingEntry.TABLE_NAME + " )";
        mDatabaseHelper.execute(query);
    }
    @Override
    public void deleteAllUnlikedTidings() {
        String selection = TidingsPersistenceContract.TidingEntry.COLUMN_NAME_FAVORITES + " NOT LIKE ?";
        String[] selectionArgs = {"1"};
        mDatabaseHelper.delete(TidingsPersistenceContract.TidingEntry.TABLE_NAME, selection, selectionArgs);
    }

    @Override
    public void likeTiding(Tiding tiding, boolean value) {
        ContentValues values = new ContentValues();
        values.put(TidingsPersistenceContract.TidingEntry.COLUMN_NAME_FAVORITES, value);

        String selection = TidingsPersistenceContract.TidingEntry.COLUMN_NAME_TITLE + " LIKE ?";
        String[] selectionArgs = {tiding.getTitle()};
        mDatabaseHelper.update(TidingsPersistenceContract.TidingEntry.TABLE_NAME, values, selection, selectionArgs);
    }

    @Override
    public Observable<Object> clearAll() {
        return Observable.create(subscriber -> {
            deleteOldPages();
            deleteAllTidings();
            subscriber.onNext(new Object());
        });
    }

    private Tiding getTiding(Cursor c) {
        String itemId = c.getString(c.getColumnIndexOrThrow(TidingsPersistenceContract.TidingEntry.COLUMN_NAME_ENTRY_ID));
        String title = c.getString(c.getColumnIndexOrThrow(TidingsPersistenceContract.TidingEntry.COLUMN_NAME_TITLE));
        String description = c.getString(c.getColumnIndexOrThrow(TidingsPersistenceContract.TidingEntry.COLUMN_NAME_DESCRIPTION));
        Long publicationDate = c.getLong(c.getColumnIndexOrThrow(TidingsPersistenceContract.TidingEntry.COLUMN_NAME_PUBLICATION_DATE));
        String link = c.getString(c.getColumnIndexOrThrow(TidingsPersistenceContract.TidingEntry.COLUMN_NAME_LINK));
        boolean isFavorites =
                c.getInt(c.getColumnIndexOrThrow(TidingsPersistenceContract.TidingEntry.COLUMN_NAME_FAVORITES)) == 1;

        return new Tiding(itemId, title, publicationDate, description, link, isFavorites);
    }

    @Override
    public Observable<List<Tiding>> getTidings() {
        String[] projection = {
                TidingsPersistenceContract.TidingEntry.COLUMN_NAME_ENTRY_ID,
                TidingsPersistenceContract.TidingEntry.COLUMN_NAME_TITLE,
                TidingsPersistenceContract.TidingEntry.COLUMN_NAME_DESCRIPTION,
                TidingsPersistenceContract.TidingEntry.COLUMN_NAME_PUBLICATION_DATE,
                TidingsPersistenceContract.TidingEntry.COLUMN_NAME_LINK,
                TidingsPersistenceContract.TidingEntry.COLUMN_NAME_FAVORITES
        };
        String sql = String.format("SELECT %s FROM %s ORDER BY %s DESC", TextUtils.join(",", projection),
                TidingsPersistenceContract.TidingEntry.TABLE_NAME, TidingsPersistenceContract.TidingEntry.COLUMN_NAME_PUBLICATION_DATE);
        return mDatabaseHelper.createQuery(TidingsPersistenceContract.TidingEntry.TABLE_NAME, sql)
                .mapToList(mTidingMapperFunction);
    }

    @Override
    public void refreshTidings() {

    }

    @Override
    public Observable<Tiding> getTiding(@NonNull String tidingId) {
        String[] projection = {
                TidingsPersistenceContract.TidingEntry.COLUMN_NAME_ENTRY_ID,
                TidingsPersistenceContract.TidingEntry.COLUMN_NAME_TITLE,
                TidingsPersistenceContract.TidingEntry.COLUMN_NAME_DESCRIPTION,
                TidingsPersistenceContract.TidingEntry.COLUMN_NAME_PUBLICATION_DATE,
                TidingsPersistenceContract.TidingEntry.COLUMN_NAME_LINK,
                TidingsPersistenceContract.TidingEntry.COLUMN_NAME_FAVORITES
        };
        String sql = String.format("SELECT %s FROM %s WHERE %s LIKE ?",
                TextUtils.join(",", projection), TidingsPersistenceContract.TidingEntry.TABLE_NAME,
                TidingsPersistenceContract.TidingEntry.COLUMN_NAME_ENTRY_ID);
        return mDatabaseHelper.createQuery(TidingsPersistenceContract.TidingEntry.TABLE_NAME, sql, tidingId)
                .mapToOneOrDefault(mTidingMapperFunction, null);
    }
}
