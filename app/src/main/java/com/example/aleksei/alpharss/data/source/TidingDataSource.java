package com.example.aleksei.alpharss.data.source;

import android.support.annotation.NonNull;

import com.example.aleksei.alpharss.data.Page;
import com.example.aleksei.alpharss.data.Tiding;

import java.util.List;

import rx.Observable;


public interface TidingDataSource {

    void saveTiding(Tiding tiding);

    void deleteAllTidings();

    Observable<List<Tiding>> getTidings();

    void refreshTidings();

    Observable<Tiding> getTiding(@NonNull String tidingId);

    Observable<Page> getPage(String url, boolean forceLoad);

    void savePage(Page page);

    Observable<List<Page>> getPages();

    void deleteOldPages();

    void deleteAllUnlikedTidings();

    void likeTiding(Tiding tiding, boolean value);

    Observable<Object> clearAll();
}
