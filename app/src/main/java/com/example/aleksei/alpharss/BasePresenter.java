package com.example.aleksei.alpharss;

public interface BasePresenter {

    void subscribe();

    void unsubscribe();

}
