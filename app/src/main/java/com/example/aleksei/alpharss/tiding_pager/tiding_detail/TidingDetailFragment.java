package com.example.aleksei.alpharss.tiding_pager.tiding_detail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.aleksei.alpharss.R;
import com.example.aleksei.alpharss.utils.InternetAvailabilityChecker;

public class TidingDetailFragment extends Fragment implements TidingDetailContract.View {

    @NonNull
    private static final String ARGUMENT_TIDING_ID = "TIDING_ID";

    private TidingDetailContract.Presenter mPresenter;

    private WebView mWebView;

    private SwipeRefreshLayout srl;

    View root;

    public static TidingDetailFragment newInstance(@Nullable String taskId) {
        Bundle arguments = new Bundle();
        arguments.putString(ARGUMENT_TIDING_ID, taskId);
        TidingDetailFragment fragment = new TidingDetailFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.tiding_detail_fragment, container, false);

        srl = (SwipeRefreshLayout) root.findViewById(R.id.refresh_layout);
        srl.setOnRefreshListener(() -> mPresenter.refreshTidings());

        mWebView = (WebView) root.findViewById(R.id.webView);

        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.setWebViewClient(new MyWebViewClient());

        mWebView.getSettings().setAppCacheMaxSize( 5 * 1024 * 1024 ); // 5MB
        mWebView.getSettings().setAppCachePath( getActivity().getApplicationContext().getCacheDir().getAbsolutePath() );
        mWebView.getSettings().setAllowFileAccess( true );
        mWebView.getSettings().setAppCacheEnabled( true );
        mWebView.getSettings().setCacheMode( WebSettings.LOAD_DEFAULT ); // load online by default
        mWebView.getSettings().setCacheMode( WebSettings.LOAD_CACHE_ELSE_NETWORK );

        return root;
    }

    @Override
    public boolean isInternetAvailable() {
        InternetAvailabilityChecker checker = new InternetAvailabilityChecker();
        return checker.isInternetAvailable(getContext());
    }

    @Override
    public void showLoadingTidingsError() {
        showMessage(getString(R.string.loading_tidings_error));
    }

    @Override
    public void showSource(String url, String source) {
        mWebView.loadDataWithBaseURL(null, source,
                "text/html; charset=utf-8", "UTF-8", null);
    }

    @Override
    public void showNoInternet() {
        showMessage(getString(R.string.no_internet_error));
    }

    private void showMessage(String message) {
        Snackbar.make(root, message, Snackbar.LENGTH_SHORT).show();
    }


    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(TidingDetailContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showMissingTiding() {
        showMessage(getString(R.string.loading_tidings_error));
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (getView() == null) {
            return;
        }
        srl.post(() -> srl.setRefreshing(active));
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

}
