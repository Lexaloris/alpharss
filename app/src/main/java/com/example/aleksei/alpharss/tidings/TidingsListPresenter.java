package com.example.aleksei.alpharss.tidings;

import android.support.annotation.NonNull;

import com.example.aleksei.alpharss.data.Tiding;
import com.example.aleksei.alpharss.data.source.TidingRepository;
import com.example.aleksei.alpharss.utils.schedulers.BaseSchedulerProvider;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class TidingsListPresenter implements TidingsListContract.Presenter {

    @NonNull
    private final TidingRepository mTidingsRepository;

    @NonNull
    private final TidingsListContract.View mTidingListView;

    @NonNull
    private final BaseSchedulerProvider mSchedulerProvider;

    @NonNull
    private CompositeSubscription mSubscriptions;

    @NonNull
    private TidingFilterType mCurrentFiltering = TidingFilterType.ALL_TIDINGS;

    public TidingsListPresenter(@NonNull TidingRepository tidingRepository,
                                @NonNull TidingsListContract.View tidingListView,
                                @NonNull BaseSchedulerProvider schedulerProvider) {
        mTidingsRepository = tidingRepository;
        mTidingListView = tidingListView;
        mSchedulerProvider = schedulerProvider;
        mSubscriptions = new CompositeSubscription();
        mTidingListView.setPresenter(this);
    }

    public enum TidingFilterType {
        ALL_TIDINGS,
        FAVORITES_TIDINGS
    }

    @Override
    public void subscribe() {
        loadTidings(false, false);
    }

    @Override
    public void unsubscribe() {
        mSubscriptions.clear();
    }

    @Override
    public void loadTidings(boolean forceUpdate) {
        if (!mTidingListView.isInternetAvailable() && forceUpdate) {
            mTidingListView.showNoInternet();
            mTidingListView.setLoadingIndicator(false);
        } else {
            switch (mCurrentFiltering) {
                case FAVORITES_TIDINGS:
                    loadTidings(false, true);
                    break;
                default:
                    loadTidings(forceUpdate, true);
            }
        }

    }

    @Override
    public void openTidingDetails(int position) {
        mTidingListView.showTidingDetailsUi(position);
    }

    @Override
    public void setFiltering(@NonNull TidingFilterType requestType) {
        mCurrentFiltering = requestType;
    }

    @Override
    public TidingFilterType getFiltering() {
        return mCurrentFiltering;
    }

    @Override
    public void like(Tiding tiding, boolean value) {
        mTidingsRepository.likeTiding(tiding, value);
        loadTidings(false, false);
    }

    @Override
    public void processToSplash() {
        mSubscriptions.clear();
        Subscription subscription = mTidingsRepository
                .clearAll()
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mTidingListView.showToSplashError();
                    }

                    @Override
                    public void onNext(Object object) {
                        mTidingListView.toSplashScreen();
                    }
                });
        mSubscriptions.add(subscription);
    }

    @Override
    public void processToProfile() {
        mTidingListView.toProfileScreen();
    }

    private void loadTidings(boolean forceUpdate, final boolean showLoadingUI) {
        if (showLoadingUI) {
            mTidingListView.setLoadingIndicator(true);
        }
        if (forceUpdate) {
            mTidingsRepository.refreshTidings();
        }

        mSubscriptions.clear();
        Subscription subscription = mTidingsRepository
                .getTidings()
                .flatMap(Observable::from)
                .filter(task -> {
                    switch (mCurrentFiltering) {
                        case FAVORITES_TIDINGS:
                            return task.isFavorite();
                        case ALL_TIDINGS:
                        default:
                            return true;
                    }
                })
                .toList()
                .subscribeOn(mSchedulerProvider.computation())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(new Subscriber<List<Tiding>>() {
                    @Override
                    public void onCompleted() {
                        if (!forceUpdate) {
                            mTidingListView.setLoadingIndicator(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mTidingListView.showLoadingTidingsError();
                    }

                    @Override
                    public void onNext(List<Tiding> tidings) {
                        if (!forceUpdate) {
                            processTidings(tidings);
                        } else {
                            loadTidings(false, true);
                        }
                    }
                });

        mSubscriptions.add(subscription);
    }

    private void processTidings(@NonNull List<Tiding> tidings) {
        if (tidings.isEmpty()) {
            processEmptyTidings();
        } else {
            mTidingListView.showTidings(tidings);
        }
        showFilterLabel();
    }

    private void processEmptyTidings() {
        switch (mCurrentFiltering) {
            case FAVORITES_TIDINGS:
                mTidingListView.showNoFavoriteTidings();
                break;
            default:
                mTidingListView.showNoTidings();

        }
    }

    private void showFilterLabel() {
        switch (mCurrentFiltering) {
            case FAVORITES_TIDINGS:
                mTidingListView.showFavoritesFilterLabel();
                break;
            default:
                mTidingListView.showAllFilterLabel();
                break;
        }
    }
}
