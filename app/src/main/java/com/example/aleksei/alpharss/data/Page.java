package com.example.aleksei.alpharss.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.UUID;

public class Page {

    @NonNull
    private final String mId;

    @Nullable
    private String mLink;

    @Nullable
    private String mSource;

    public Page(@Nullable String link, @Nullable String source) {
        mId = UUID.randomUUID().toString();
        mLink = link;
        mSource = source;
    }

    public Page(@NonNull String itemId, @Nullable String link, @Nullable String source) {
        mId = itemId;
        mLink = link;
        mSource = source;
    }

    @NonNull
    public String getId() {
        return mId;
    }

    @NonNull
    public String getLink() {
        return mLink;
    }

    @NonNull
    public String getSource() {
        return mSource;
    }

}
