package com.example.aleksei.alpharss.data.source.remote.parser;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

@Root(name = "item", strict = false)
public class RssAlphaItem implements Serializable {

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public String getLink() {
        return link;
    }

    @Element(name = "title", required = true )
    private String title;

    @Element(name = "pubDate", required = true )
    private String publicationDate;

    @Element(name = "description", required = true )
    private  String description;

    @Element(name = "link", required = true )
    private  String link;

    public RssAlphaItem(String title, String description, String publicationDate, String link) {
        this.title = title;
        this.description = description;
        this.publicationDate = publicationDate;
        this.link = link;
    }
    public RssAlphaItem() {}

    protected boolean isEqualTo(RssAlphaItem o) {
        return o.getTitle().equals(title) &&
                o.getDescription().equals(description) &&
                o.getPublicationDate().equals(publicationDate) &&
                o.getLink().equals(link);
    }
}
